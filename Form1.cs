﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MouseMove
{
    public partial class MoveIT : Form
    {

 
        public MoveIT()
        {
            InitializeComponent();
        }
        
        //private static void MoveMouse()
        //{
        //    Point curPos = Cursor.Position;
        //   Cursor.Position = new Point(curPos.X + 25, curPos.Y + 25);
        //}

        private static void AppActivate()
        {
           
            
            using (Process xProcNotePad = new Process())
            {

                xProcNotePad.StartInfo.FileName="Notepad.exe";
               // xProcNotePad.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                xProcNotePad.Start();
                xProcNotePad.WaitForInputIdle();
                SendKeys.SendWait("{UP}");
                xProcNotePad.Kill();
            }

       
        }

        int xTimes = 0;
        Point mouseLastPoint = Cursor.Position;  // Captures mouse's last coordinates to see if mouse has moved (if it hasn't moved, then process commands)
  
        private void timer1_Tick(object sender, EventArgs e)
        {
            // Triggers every 4 minutes.
            if (Cursor.Position  ==  mouseLastPoint)
            {

                AppActivate();

                xTimes += 1;
                notifyIcon1.Icon = SystemIcons.Exclamation;
                notifyIcon1.BalloonTipTitle = "MoveIT";
                notifyIcon1.BalloonTipText = "Moved: " + DateTime.Now.ToString() + String.Format(" [{0} Times]", xTimes);
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(3000);
            }

            mouseLastPoint = Cursor.Position;

        }

        private void toolStripMenuItem_EXIT_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MoveIT_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void MoveIT_Shown(object sender, EventArgs e)
        {
            Visible = false;

        }


    


    }
}
